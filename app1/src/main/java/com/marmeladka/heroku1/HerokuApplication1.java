package com.marmeladka.heroku1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@RestController
@RequestMapping(path = "app/v1")
public class HerokuApplication1 {

    public static void main(String[] args) {
        SpringApplication.run(HerokuApplication1.class, args);
    }

    @GetMapping
    public String undefined() {
        return "Please complete the request to switch to other page";
    }

    @GetMapping("/hello")
    public String hello1(@RequestParam(name = "name", defaultValue = "World") String name) {
        return "Hello, " + name + " from module 1 !";
    }

}
