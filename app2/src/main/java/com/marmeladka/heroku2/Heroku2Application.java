package com.marmeladka.heroku2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients
@RestController
@RequestMapping(path = "app/v2")
public class Heroku2Application {

    @Autowired
    private FeignClientService feignClientService;

    public static void main(String[] args) {
        SpringApplication.run(Heroku2Application.class, args);
    }

    @GetMapping
    public String undefined() {
        return "Please complete the request to switch to other page";
    }

    @GetMapping("/hello")
    public String hello2(@RequestParam(name = "name", defaultValue = "World") String name) {
        return "Hello, " + name + " from module 2 !";
    }

    @GetMapping("/connect")
    public String helloFromOtherService(String name) {
        return feignClientService.hello1(name) + " from other service";
    }

}