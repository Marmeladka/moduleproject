package com.marmeladka.heroku2;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "module-app-1", url = "${other.instance.url}")
public interface FeignClientService {

    @GetMapping("app/v1/hello")
    String hello1(@RequestParam(name = "name", defaultValue = "World") String name);

}
